<?php

namespace Inferno;

use App\Core\Network\Network;
use App\Core\Request\Request;
use Inferno\Debug\ErrorHandler;
use Inferno\Environment\Constants;
use Inferno\Environment\Environment;
use Inferno\Request\Router;
use Inferno\Response\Response;
use Inferno\View\View;

class Application {

    public static $basePath;
    public static $env;
    public static $routes = [];

    public function __construct($basePath) {
        self::$basePath = $basePath;

        Constants::register();
        Environment::register();
        ErrorHandler::register();

        Request::register();
        Router::register();
        Response::register();

        require "Collection/helpers.php";

        View::register();
        Router::dispatch();
    }

}
