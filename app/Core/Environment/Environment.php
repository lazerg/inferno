<?php

namespace Inferno\Environment;

use Dotenv\Dotenv;
use Inferno\Application;

class Environment {

    public static function register() {
        Application::$env = Dotenv::createImmutable(ROOT)->load();

        define("DEBUG", Application::$env == true);

        require_once "helpers.php";
    }

}
