<?php

namespace Inferno\Environment;

use Inferno\Application;

class Constants {

    public static function register() {
        define('ROOT', Application::$basePath);
        define('APP', ROOT . '/app');
        define('VIEWS', ROOT . '/resources/views');
        define('ASSETS', ROOT . '/public/assets');
        define('DEFAULT_VIEWS', VIEWS . '/default');
        define('URL', trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/'));
        define('METHOD', $_SERVER['REQUEST_METHOD']);
        define('CONTROLLER', ROOT . '/app/Controllers');
    }

}
