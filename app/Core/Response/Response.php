<?php

namespace Inferno\Response;

class Response {

    public $content;

    public static function register() {
        require "helpers.php";
    }

    public function __construct($content = '') {
        $this->content = $content;
    }

    public function json($data = [], $status = 200, array $headers = []) {
        if ($this->content !== '') return $this->content;

        header('Content-Type: application/json');
        foreach ($headers as $header) header($header);
        http_response_code($status);
        return json_encode($data);
    }

}
