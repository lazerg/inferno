<?php

function view($view, $variables = []) {
    foreach ($variables as $k => $variable) $$k = $variable;

    ob_start();
        include(VIEWS . "/$view.inferno.php");
        $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function response($content = '') {
    return new Inferno\Response\Response($content);
}
