<?php

namespace Inferno\Request;

use Inferno\Application;

class Router {

    public static function register() {
        require_once (APP . "/routes.php");
    }

    public static function addRoute($url, $controller, $action, $method) {
        array_push(Application::$routes, [
            'url' => $url,
            'controller' => $controller,
            'action' => $action,
            'method' => $method,
        ]);
    }

    public static function dispatch(){
        foreach (Application::$routes as $route){
            if ($route['method'] == METHOD && $route['url'] == URL){
                $path = "App\\Controllers\\" . $route['controller'];
                if (!class_exists($path)) die("Controller $path does not exists");
                $action = $route['action'];
                $controller = new $path();
                echo $controller->$action();
                die();
            }
        }

        http_response_code(404);
        require_once (DEFAULT_VIEWS . "/404.php");
    }

}
