<?php

namespace App\Core\Request;

class Request {

    public static $data = [];

    public static function register() {
        foreach ($_GET as $k => $v) {
            array_push(self::$data, [
                'method' => 'get',
                'key' => $k,
                'value' => $v
            ]);
        }

        foreach ($_POST as $k => $v) {
            array_push(self::$data, [
                'method' => 'post',
                'key' => $k,
                'value' => $v
            ]);
        }
    }

    public static function all() {
        $return = [];

        foreach (self::$data as $v) {
            $return[$v['key']] = $v['value'];
        }

        return $return;
    }

    public static function get($key) {

        foreach (self::$data as $v) {
            if ($v['key'] === $key && $v['method'] === 'get') {
                return $v['value'];
            }
        }
    }

    public static function post($key) {

        foreach (self::$data as $v) {
            if ($v['key'] === $key && $v['method'] === 'post') {
                return $v['value'];
            }
        }
    }

}
