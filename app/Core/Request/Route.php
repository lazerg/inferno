<?php

namespace Inferno\Request;

class Route {

    public static function get($url, $action) {
        $temp = explode("@", $action);
        $controller = $temp[0];
        $action = $temp[1];
        $url = trim($url, "/");

        Router::addRoute($url, $controller, $action, 'GET');
    }

}
