<?php

namespace Inferno\Debug;

class ErrorHandler {
    public static function register(){
        (new \Whoops\Run)
            ->pushHandler(new \Whoops\Handler\PrettyPageHandler())
            ->register();
    }
}
