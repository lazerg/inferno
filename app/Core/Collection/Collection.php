<?php

namespace Inferno\Collection;

use App\Core\Collection\CollectionAlgorithms;

class Collection
{
    use CollectionAlgorithms;

    protected $items = [];

    public function __construct(array $items) {
        $this->items = $items;
    }

    public function toArray(){
        return $this->items;
    }

    public function toJson(){
        return json_encode($this->items, true);
    }
}
