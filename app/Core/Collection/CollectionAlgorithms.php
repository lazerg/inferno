<?php

namespace App\Core\Collection;

trait CollectionAlgorithms
{
    public function map(callable $callback) {
        $this->items = array_map($callback, $this->items);

        return $this;
    }

    public function where($key, $operator, $value = null) {
        if ($value === null) {
            $value = $operator;
            $operator = '==';
        }

        $this->items = array_filter($this->items,
            function ($item) use ($key, $operator, $value) {

                switch ($operator) {
                    case '==':
                    case '=':
                        return $item[$key] == $value;
                        break;

                    case '!==':
                    case '!=':
                        return $item[$key] !== $value;
                        break;

                    case '>=':
                        return $item[$key] >= $value;
                        break;

                    case '>':
                        return $item[$key] > $value;
                        break;

                    case '<=':
                        return $item[$key] <= $value;
                        break;

                    case '<':
                        return $item[$key] < $value;
                        break;
                }

                return true;
            }
        );

        return $this;
    }
}
