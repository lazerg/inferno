<?php

use Inferno\Collection\Collection;

function collect($value = []) {
    return new Collection($value);
}
