<?php

namespace App\Controllers;

class HomeController extends Controller {

    public function index() {
        return view('index');
    }

    public function postsJson() {
        return response()->json([
            'title' => 'Why Inferno is great?',
            'body' => 'Because it is created by @lazerg)'
        ]);
    }

}
