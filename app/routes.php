<?php

use Inferno\Request\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/json', 'HomeController@postsJson');
